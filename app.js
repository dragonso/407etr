const fs = require('fs');
const RATE = 0.25;
const DECIMAL_PLACES = 2;

const EXCHANGES = JSON.parse(
    fs.readFileSync('interchanges.json','utf-8'))['locations'];

const RATES = JSON.parse(
        fs.readFileSync('rates.json','utf-8'))['rates'];

function getIndex(exchangeName){
    let res =  Object.entries(EXCHANGES)
    .find(([_,val]) => {
        return val.name == exchangeName
        }
    )
    return res ? res[0]:null;
}

function getDistance(isEastBound,currentExchangeIndex,targetExchangeIndex){
    if(currentExchangeIndex == targetExchangeIndex){
        return 0;
    }
    let routes = Array.from(EXCHANGES[currentExchangeIndex]['routes']);
    let route = isEastBound ? routes.find(r => r.toId > currentExchangeIndex): routes.find(r => r.toId < currentExchangeIndex);
    if(!route){
        return null;
    } 
    return route.distance + getDistance(isEastBound,route.toId,targetExchangeIndex);
}

function getRatesPerVehicleType(vehicleType){
    let res = Object.entries(RATES)
    .find(([key,_]) => {
        return key == vehicleType
        });
    return res ? res[1]:null;
}

function calculateRate(fromExchange,toExchange){
    let fromIndex = getIndex(fromExchange);
    let toIndex = getIndex(toExchange);
    let isEastBound = fromIndex < toIndex;
    let distance = getDistance(isEastBound,fromIndex,toIndex);
    return {distance:distance,fee:Number(RATE * distance).toFixed(DECIMAL_PLACES)}
}

function calculateRatePerVehicleType(fromExchange,toExchange,vehicleType){
    let initCalcRes = calculateRate(fromExchange,toExchange);
    let fromIndex = getIndex(fromExchange);
    let toIndex = getIndex(toExchange);
    let isEastBound = fromIndex < toIndex;
    let direction = isEastBound? 'eastbound':'westbound';
    let vehicleSpecificRates = getRatesPerVehicleType(vehicleType);
    let trip_toll_charge = vehicleSpecificRates['trip_toll_charge'];
    let camera_charge = vehicleSpecificRates['camera_charge'];
    let perKMRate = vehicleSpecificRates[direction];
    return {direction:direction,kmRate:perKMRate,distance:initCalcRes.distance,
        tripCharge: Number(trip_toll_charge + camera_charge + perKMRate * initCalcRes.distance).toFixed(DECIMAL_PLACES)}

}
console.log("Sample input 1: QEW,Dundas Street")
console.log("Sample output")
console.log(calculateRate('QEW','Dundas Street'));
console.log("Sample input 2: heavy,Dundas Street,QEW")
console.log("Sample output")
console.log(calculateRatePerVehicleType('Dundas Street','QEW','heavy'));
let stdin = process.openStdin();
console.log("awaiting input")
stdin.addListener("data", function(d) {
    let input = d.toString().trim();
    let args = input.split(',');
    if(args.length == 2){
        console.log(args);
        let res = calculateRate(args[0],args[1]);
        console.log("Distance: "+res.distance);
        console.log("Cost:" + res.fee);
    }else if(args.length == 3){
        console.log(args);
        console.log(calculateRatePerVehicleType(args[1],args[2],args[0]));
    }else{
        console.warn("please check your input");
    }
    
  });
