# 407etr

### Software requirements
Node V10+ (tested with v13.7.0)

### How to run
check repo
run 'node app.js'

### Sample input and output
from,to
vehicleType,from,to
Sample input 1: QEW,Dundas Street
Sample output
{ distance: 6.062, fee: '1.52' }
Sample input 2: heavy,Dundas Street,QEW
Sample output
{
  direction: 'westbound',
  kmRate: 0.7416,
  distance: 6.062,
  tripCharge: '51.50'
}

### Preconditions
from, to, and vehicle type must exist in exchanges.json and rates.json respectively


